view: history {
 sql_table_name: demo_db.orders ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.status ;;
    tags: ["email"]
  }

  dimension: EMAIL {
    type: string
    sql: ${TABLE}.status ;;
    tags: ["EMAIL"]
  }

  dimension: user_id {
    type: number
    sql: ${TABLE}.user_id ;;
    tags: ["user_id"]
  }

  dimension: userId {
    type: number
    sql: ${TABLE}.user_id ;;
    tags: ["userId"]
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  measure: most_recent_order {
    type: date
    sql: MAX(${created_date}) ;;
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      users.id,
      users.first_name,
      users.last_name,
      billion_orders.count,
      fakeorders.count,
      hundred_million_orders.count,
      hundred_million_orders_wide.count,
      order_items.count,
      ten_million_orders.count
    ]
  }
  }
