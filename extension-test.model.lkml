connection: "thelook"
# include all the views
include: "views/*.view"

datagroup: default {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: default

explore: linna_test_497482 { ##Added by Chai 03/15/2021
  ##hidden: yes
  persist_with: default
  view_name: history
}
